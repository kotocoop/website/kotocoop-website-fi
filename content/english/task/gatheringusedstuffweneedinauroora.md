---
title: Gathering used stuff we need in Auroora
date: 2023-10-11 10:00:00
layout: singletask
aliases:
- /task/gatheringusedstuffweneedinauroora
---

Our goal is to compile a list of items that are currently needed in Auroora and that people can send there. The idea is that some people may already have some of these items that they no longer need, or they can find them at second-hand shops.

We need a list, organization and a social media post about it.

## About the task

Ask Juuso V (@jeukku) for editing rights.

# Content

kitchen appliances list

curtains

- Kori maustepusseille
- Villasukkia tai vastaavia
- Mattoja
- Verhoja
- OK -  Kahvinkeitin
- Vedenkeitin
- Mikroaaltouuni
- Aamupalakippoja/syviä lautasia
- Juomalaseja
- Baarijakkaroita
- Lajittelu/kierrätys korit/pussit/laatikot
- Kaulin
- Hattuhylly
- Kenkäteline
- Työkaluja kuten vasarat ja puukot
- Puutarhatyökaluja



