---
title: Our social media pages and chats
date: 2021-12-26 10:30:00 +0000
aliases:
  - /contact
tags: []
images:
  - /img/blog/oursocialmediapages.jpg
related_posts: []
enable_contact_form: true
---
## Alternative platforms

While current social media sites can be important to reach people, we should be using use open-source alternatives. Please send us some suggestions what platforms to use!

## Matrix Spaces

Good alternative for Discord and other chats is Matrix. Element.io is seems to currently the best client out there. You can find our rooms here <https://matrix.to/#/#public:matrix.kotocoop.org>

## Here are our social media sites

Facebook: <https://facebook.com/kotocoop>

Twitter: <https://twitter.com/kotocoop>

Peertube/Videos: - <https://tube.kotocoop.org/videos/>

Instagram: <https://www.instagram.com/kotocoop>

Suomenkielinen Instagram: <https://www.instagram.com/osuuskuntaelokoto>

## Here are chats where you can find us

Telegram channel and group chat: <https://t.me/joinchat/HJFMZkMm1Iqp5sy_t6IVQw>

Matrix/Element: <https://matrix.to/#/#public:matrix.kotocoop.org>

Discord chat server: <https://discord.gg/7eVJFCG>

## Other options

### Scuttlebutt

What great platform is <https://scuttlebutt.nz/>

Get connected and find people through #kotocoop channel. If you need help, contact Jeukku (<https://room.kotocoop.org/alias/jeukku> or jeukku at Telegram) or just use our contact form on the website.
