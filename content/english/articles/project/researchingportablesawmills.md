---
title: Project report - Researching portable sawmills
date: 2024-03-02 20:27:00 +0000
aliases:
- /articles/project/portablesawmills
images:
- /img/articles/techproject.png
tags: ["project", "tech", "construction"]
---

This project was created mostly to support renovation in Auroora, but it provided valuable information for the future too. We needed to decide if we could use a portable sawmill to cut timber and woodplanks.

{{< rawhtml >}}

<br>
<h2>Types</h2>
<br>
<h3>Chainsaw mill</h3>
<br>
<img class="img-fluid"  src=
"https://www.uittokalusto.fi/media/catalog/product/cache/a6f38f6af1979b81e83b5c2dba7f5948/e/3/e3820-alaskan-small-log-mill-2.jpg">
<br>
<br>
<h4>What is needed</h4>
<br>
<a href=
"https://www.puuilo.fi/norrkoping-moottorisahan-ohjain-puun-halkaisuun"
rel=
"noreferrer noopener">https://www.puuilo.fi/norrkoping-moottorisahan-ohjain-puun-halkaisuun</a>
119€ or&nbsp;<br>
<a href=
"https://www.uittokalusto.fi/granberg-alaskan-small-log-mill-sahausohjain-2.html"
rel=
"noreferrer noopener">https://www.uittokalusto.fi/granberg-alaskan-small-log-mill-sahausohjain-2.html</a>
249€<br>
<br>
Electric chainsaw <a href=
"https://www.netrauta.fi/ketjusaha-ryobi-rcs2340b-2300w-40cm" rel=
"noreferrer noopener">https://www.netrauta.fi/ketjusaha-ryobi-rcs2340b-2300w-40cm</a>
134€. Make sure that the saw is long enough. You can buy different
sizes of bars and chains later too.<br>
<br>
Use a chain that is designed for the job <a href=
"https://www.uittokalusto.fi/oregon-halkaisuketju.html" rel=
"noreferrer noopener">https://www.uittokalusto.fi/oregon-halkaisuketju.html</a><br>

<br>
<h4>How is it used?</h4>
<br>
Here is a pretty good video how it is used <a href=
"https://www.youtube.com/watch?v=JC8NSu8YqWQ" rel=
"noreferrer noopener">https://www.youtube.com/watch?v=JC8NSu8YqWQ</a>
and another one <a href=
"https://www.youtube.com/watch?v=HbAwkID7cjA" rel=
"noreferrer noopener">https://www.youtube.com/watch?v=HbAwkID7cjA</a>.
Following images are screenshots of those videos.<br>
<br>
Ladders are used to support the first cut:<br>
<img class="img-fluid"  src=
"https://content.kotocoop.org/pad/z4cm8t0zzn2htopk/a601dfa9-62be-4f34-a735-cc4f9f9455d3.png">
<br>
<br>
Cut two sides first and try to make it square:<br>
<img class="img-fluid"  src=
"https://content.kotocoop.org/pad/z4cm8t0zzn2htopk/2705c909-7a24-4f5b-8485-f7cb25a569b7.png">
<br>
<br>
Adjust the jig for wanted thickness and cut planks using the cut as
a support&nbsp;<br>
<img class="img-fluid"  src=
"https://content.kotocoop.org/pad/z4cm8t0zzn2htopk/fc09083f-ee42-4fab-b695-3bf7154dff67.png">
<br>
<br>
using ladders for every cut seems more safe (starting and ending
the cut when ladders are longer than the log). Also these guys are
using a weight to assist the saw:<br>
<img class="img-fluid"  src=
"https://content.kotocoop.org/pad/z4cm8t0zzn2htopk/14c24709-0e25-4a91-b157-7d090bef3708.png">
<br>
<br>
<br>
<h4>How much time it takes to use?</h4>
<br>
Setting up the first cuts takes time. It is hard to guess how long,
but maybe 20min. Looks like you can cut maybe a meter in a
minute.<br>
<br>
<h4>How safe is it?</h4>
<br>
The Chainsaw is attached to the rig. It seems safe if you are
carefull with moving the logs and don't hurry setting everything
up.<br>
<br>
<h4>Pros/Cons</h4>
<br>
The system is cheap, but cutting can be heavy and slow.<br>
<br>
You can cut logs as long as you get the first cut straight so most
likely how long the ladders are.<br>
<br>
With chainsaw mill you can cut the trees, on the ground, where they
are cut down.<br>
<br>
<h3>Bandsaw mill</h3>
<br>
<a href=
"https://www.uittokalusto.fi/norwood-lumberman-mn27-tukkivannesaha.html"
rel=
"noreferrer noopener">https://www.uittokalusto.fi/norwood-lumberman-mn27-tukkivannesaha.html</a>
5990€. Cheapest new one I found was 3099€<br>
<img class="img-fluid"  src=
"https://content.kotocoop.org/pad/z4cm8t0zzn2htopk/df15492b-f494-4c5b-bfa4-c3a480b2803b.png">
<br>
<br>
Used ones aren't much cheaper.<br>
<br>
Building one might be an option <a href=
"https://www.youtube.com/watch?v=F09yI4f-bf4" rel=
"noreferrer noopener">https://www.youtube.com/watch?v=F09yI4f-bf4</a><br>

<br>
<h4>How is it used?</h4>
<br>
<a href="https://www.youtube.com/watch?v=72j4FiyCHCc" rel=
"noreferrer noopener">https://www.youtube.com/watch?v=72j4FiyCHCc</a><br>

<br>
<h4>How much time it takes to use?</h4>
<br>
It doesn't seem to be much faster the a chainsaw&nbsp; if at all.
Less setting up.<br>
<br>
<h4>How safe is it?</h4>
<br>
Looks really safe.<br>
<br>
<h4>Pros/Cons</h4>
<br>
Espensive. Blades can be expensive to take care.<br>
<br>
Not that hard (physically) to use, safer and setting up easier.<br>
<br>
Requires an even platform and moving from place to place needs
work.<br>
<br>
Less waste because the cut is thinner.<br>
<br>
<h2>Other things needed to work</h2>
<br>
<h3>Moving and turning the logs</h3>
<br>
We don't have heacy machines like a tractor to move and lift logs
so other solutions are needed.<br>
<br>
4m log weights somewhere about 320kg (<a href=
"http://kuukuna.net/masinistit/8783.html" rel=
"noreferrer noopener">http://kuukuna.net/masinistit/8783.html</a>).
It depends on the thickness of course.<br>
It would be nice to get over 4m long timber (Looks like 4,2m is
enough for a floor plane).<br>
<br>
Log is never lifted straight up, but dragged in the ground
mostly.<br>
<br>
<h4>Winches, pulleys</h4>
<br>
<a href="https://www.herkesoy.fi/tuote/21333640" rel=
"noreferrer noopener">https://www.herkesoy.fi/tuote/21333640</a>
149€ 400kg 10m/min length 12m.<br>
<br>
<h3>Work area</h3>
<br>
Neither type of mill requires a a lot of space or structures. For
bandsaw mill it takes more work to install it in place. It is nicer
if the log is put higher when using a chainsaw mill, but it doesn't
have to be. For chainsaw mill you use just a couple of tree
stumps.<br>
<br>
The log should stay safely in place.<br>
<br>
<h2>Other things</h2>
<br>
Plain Sawn vs Quarter Sawn <a href=
"https://www.youtube.com/watch?v=sVBsA1KbfY8" rel=
"noreferrer noopener">https://www.youtube.com/watch?v=sVBsA1KbfY8</a><br>

How to and what to cut will be in another task.<br>
<br>
<h1>Conclusions</h1>
<br>
Cheapest options is a chainsaw mill and make planks and timber
where the tree has fallen. Moving the logs requires machinery or
winches.<br>
<br>
Without any other machinery, carrying the planks is hard work too
because planks will be quite thick and not yet dried.<br>
<br>
For us a chainsaw mill is currently the best option. Because it is
cheap, we can maybe make other purchases that help with the
work.<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
{{< /rawhtml >}}
