---

title: Veganism and Koto 

date: 2021-07-15 15:30:00 +0000 

images:
- /img/blog/sheep.jpg

tags: ["earth_restoring_lifestyle"]

---

We in Koto Co-op understand that a vegan lifestyle is more ethical toward other beings and statistically much more ecological.

Veganism is all about reducing unnecessary suffering. Vegans extend the concept of suffering to all animals including humans. They accept that fostering compassion and mitigating the suffering we cause is more important than to satisfy our taste buds or habits, or to avoid slight discomfort.

For these reasons Koto encourages vegan lifestyle. However Koto does not require people in the Koto community to be vegan, but we aim to educate them about the facts about effects of different diets and lifestyles.

Veganism is more than what many vegans live by. Koto acknowledges that many who don't follow vegan diet do more to reduce suffering and ecological problems in the world.

Today, society promotes an omnivore diet. By changing the foundational structure of our society we hope to aid the public in making more conscious choices that coincide with their believes. Koto does not force anyone to act in a way they don't want to, but we try to educate and guide people to act in a way that is sustainable based on the scientific principles.

The Koto community will produce most likely only vegan food, because a vegan diet is a very important aspect to most of our members and our community should reflect values of the people it consist of. Different world-views however, should be respected even if you disagree with them. Changing your opinions even if the facts are against you requires effort and time and we have to understand that.

Some or most (or maybe in the near future all of the) units can be completely "vegan". It is a question of rules for the unit. Some people would like to have pets or rescue a goat to live in a unit with them. What is permitted by the unit rules, is up the people founding it.

The rapid expansion plan of Koto will most likely create situations where not everyone in Koto will be vegan. Koto will strive to spread information and live by example whilst helping people, both in terms of a vegan lifestyle, and other societal and technical aspects.