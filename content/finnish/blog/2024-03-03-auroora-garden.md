---
title: Aurooran puutarhan suunnittelu
date: 2024-03-04 20:00:00 +0000
layout: post
extimages:
  - https://content.kotocoop.org/pad/tayr57s3mx303rositjssgltl3ij0h3fff1l93gvt1hkbo/7917c837-6f29-42ec-ba98-c9c03e2b928e.png
tags:
  - auroora
  - earth_restoring_lifestyle
---
### Aurooran puutarhan suunnittelu ja seuranta

Seuraamme istuttamiamme kasveja ja yllä olevassa kuvassa näkyvää kokonaissuunnitelmaa.

Tutustu siihen [täällä!](https://kotocoop.org/units/auroora/garden/)

[Tietoja paikasta löytyy täältä](https://kotocoop.org/units/auroora/).
