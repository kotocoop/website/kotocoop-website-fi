---
layout: post
title: 'Auroora 2023 - Tilojen esittely - Presentation of living spaces'
sub_heading: ''
date: 2023-03-18 8:00:00 +0000
tags: [ "auroora" ]
related_posts: []

images:
  - /img/blog/auroora-2023-introduction.jpg
skip_image: true

embed_videos:
  - https://tube.kotocoop.org/videos/embed/54e1ec53-fa22-4667-92f1-f2c1db8dccd2

---


Tässä video jossa esitellään osaa käytettävissä olevista tiloista!