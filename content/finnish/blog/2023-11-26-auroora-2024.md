---
title: Auroora 2024
date: 2023-11-26 18:30:00 +0000
layout: post
tags:
  - auroora
images:
  - /img/blog/auroora2024.jpg
---
> English version below.

### Asumaan Aurooraan 2024

Yhteisömme keskittyy luomaan niukkuuden jälkeistä lahjataloutta, ja olemme sitoutuneet tukemaan toisiamme löytämään tarkoituksen ja merkityksen työssämme. Rakennamme yhteisöä, joka perustuu kestävyyden, avoimuuden, permakulttuurin, vegaaniuden, tieteellisen maailmankuvan ja avoimen lähdekoodin teknologian periaatteisiin. Lue lisää täällä <https://kotocoop.org/model/>

Nyt etsimme jotakuta asuttamaan entistä kyläkouluamme, Aurooraa, kasvukauden ajaksi (esim. puoleksi vuodeksi Toukokuusta Lokakuuhun) maksa-mitä-pystyt periaatteella. Tarjoamme käyttöön yhden ison makuuhuoneen (33 neliötä, sänky, kaappi, työpöytä), yhteiskäytössä olevat keittiön, ruokailutilan, suihkutilan ja WC:n, sekä henkilöauton ja sähköpyörän.

Lisää tietoa paikasta löytyy täältä: https://kotocoop.org/units/auroora/

Vuoden aikana taloa kunnostetaan, pihaa ja puutarhaa perustetaan.

Koko piha-alue käytettäväksi osuuskunnan kanssa yhteistyössä puutarhan perustamiseen ja muuhun toimintaan. Sato minkä koet omaksesi jää sinulle.

Kaikkien asukkaiden tulee sitoutua yhteisön sääntöihin jotka liittyvät lähinnä muiden asukkaiden huomioonottamiseen ja vastuullisuuteen ympäristöstä.

Vierailijoita sekä projektin edistämiseksi tekeviä ihmisiä asuu kokoajan tai ajoittain muissa huoneissa ja toivommekin asumaan ainakin yhtä henkilöä joka osaa ajaa autoa ja voi auttaa kuljetustehtävissä, kuten ihmisten hakemisessa juna-asemalta aina kun muita ajotaitoisia ei ole paikalla.

Jos olet kiinnostunut liittymään yhteisöön kasvukaudeksi, täytä tämä lomake https://kotocoop.org/auroora2024 niin otamme yhteyttä, kerromme lisää ja voimme keskustella yksityiskohdista.
