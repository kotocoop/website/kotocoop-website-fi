---
title: Koto osuuskunnan säännöt
date: 2020-08-02 11:30:00 +0000
---
### 1 § Toiminimi ja kotipaikka

Osuuskunnan nimi on Koto osuuskunta ja kotipaikka Hämeenlinna.

### 2 § Osuuskunnan toiminnan tarkoitus ja toimiala

Toiminnan tarkoituksena on tukea ja helpottaa jäseniensä toimeentuloa tarjoamalla heille tuotteita ja palveluita.

Tarkoituksensa toteuttamiseksi osuuskunta tarjoaa seuraavia palveluita: Tori- ja markkinakauppa, insinööripalvelut ja niihin liittyvä tekninen konsultointi, kasvinviljely ja siihen liittyvät palvelut, asuin- ja muiden rakennusten rakentaminen, sähkön tuotanto vesi- ja tuulivoimalla.

Osuuskunta voi myydä palveluita sekä tuotteita myös osuuskunnan ulkopuolelle.

### 3 § Ylijäämän jakaminen

Kaikki ylijäämä käytetään osuuskunnan laajenemiseen.

### 4 § Jäsenyys

Osuuskunnan jäseneksi pääsee maksamalla osuusmaksun. Kaikilla jäsenillä on äänioikeus osuuskuntakokouksessa.

### 5 § Osuusmaksu

Osuuskunnan hallitus määrittelee osuusmaksun vuosittain.

### 6 § Hallitus

Osuuskunnan hallitukseen kuuluu puheenjohtaja sekä yksi varajäsen.

Hallituksen toimikausi on toistaiseksi voimassa oleva.

Uusi hallitus valitaan siten, että virkaatekevä hallitus tekee esityksen uuden hallituksen kokoonpanosta. Kokoonpano hyväksytään osuuskunnan kokouksessa.

### 7 § Toiminimen kirjoittaminen

Osuuskunnan nimenkirjoittajana toimii hallituksen puheenjohtaja.

### 8 § Osuuskunnan kokous

Jäsenet käyttävät päätösvaltaansa sille lain tai sääntöjen mukaan kuuluvissa asioissa osuuskunnan kokouksissa.

Kokous pidetään osuuskunnan kotipaikassa. Kokous voidaan pitää myös muulla paikkakunnalla, jossa osuuskunnalla on toimipaikka.

Osuuskunnan kokouksen koolle kutsumisessa, kokouskutsun sisällössä, kokouskutsuajassa ja kutsutavassa sekä kokousasiakirjojen nähtävillä pitämisessä ja lähettämisessä noudatetaan osuuskuntalain 5 luvun määräyksiä. Kokouskutsu lähetetään sähköpostilla jäsenen ilmoittamaan sähköpostiosoitteeseen tai muulla sähköisellä tavalla.

### 9 § Varsinainen osuuskunnan kokous

Varsinainen osuuskunnan kokous on pidettävä kuuden kuukauden kuluessa tilikauden päättymisestä

Kokouksessa on päätettävä:

\    1. tilinpäätöksen vahvistamisesta

\    2. taseen osoittaman ylijäämän käyttämisestä

\    3. vastuuvapauden myöntämisestä hallituksen jäsenille

\    4. osuuskunnan toiminnantarkastaja

### 10 § Tilikausi ja tilinpäätös

Osuuskunnan tilikausi on kalenterivuosi. Kultakin tilikaudelta on laadittava tilinpäätös. Tilinpäätös on annettava tilintarkastajalle vähintään kuukautta ennen sitä kokousta, jossa tuloslaskelma ja tase esitetään vahvistettaviksi.

11 § Tilintarkastaja ja toiminnantarkastaja

Osuuskunnalle valitaan yksi tilintarkastaja ja varatilintarkastaja, jos tilintarkastuslaki sitä edellyttää. Tilintarkastajan toimikausi on toistaiseksi voimassa oleva.

Osuuskunnalla on oltava osuuskunnan kokouksen valitsema toiminnantarkastaja ja toiminnantarkastajan sijainen, jos osuuskunnassa ei ole tilintarkastajaa.

### 12 § Yksiköt

Yksikkö on nimetty ryhmä osuuskunnan jäseniä jotka noudattavat osuuskunnan asettamia yksikön sääntöjä.

Yksikkö valitsee keskuudestaan vastuuhenkilön joka toimii yhteyshenkilönä ja on velvollinen valvomaan yksikön sääntöjen noudattamista sekä raportoimaan osuuskunnan hallitukselle toiminnasta yksikön sääntöjen mukaisesti.

Kaikki osuuskunnan jäsenet kuuluvat yhteen yksikköön.

Jäsenet voivat pyytää siirtoa toiseen yksikköön ja sekä osuuskunnan hallituksen, että yksikön yhteyshenkilön tulee hyväksyä siirto.

Osuuskunta voi halutessaan purkaa yksikön jolloin yksikköön kuuluvat jäsenet erotetaan.

### 13 § Sääntöjen muuttaminen

Sääntöjen muuttamisesta päättää osuuskunnan kokous. Hallitus hyväksyy sääntömuutokset.

### 14 § Osuuskunnan purkaminen

Päätös osuuskunnan vapaaehtoisesta purkamisesta ja selvitystilaan asettamisesta tekee hallitus.

### 15 § Yleismääräys

Muilta osin noudatetaan voimassa olevaa osuuskuntalakia.
