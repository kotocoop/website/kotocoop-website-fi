---

title: Koto Co-op organization structure and guidelines

---


## Perusteet

Koto Osuuskunnan organisatorisen rakenteen ydin on vaakasuoran valtahierarkian, tiimien ja yksiköiden itseorganisoinnin ja autonomian säilyttäminen samalla, kun osuuskunnan talous pidetään kestävällä pohjalla. Perussääntönä on, että osuuskunnan hallinto ei ohjaa tiimejä tai yksiköitä, mutta tarvittaessa se puuttuu toimintaan. Osuuskunnan hallinto käsittelee Koto-projektin varallisuutta ja julkisia suhteita, mutta muutoin tiimit ja yksiköt voivat järjestäytyä mielensä mukaan. Jos rahoihin tai ihmisten välisiin konflikteihin liittyvä ongelma ei ratkea ilman osuuskunnan hallinnon väliintuloa, se pyrkii ratkaisemaan nämä ongelmat.


## Yksiköiden Organisointi

Koto-osuuskunnan yksiköiden organisoitumisen ydinajatus on, että ne määrittelevät omat sääntönsä yhdessä osuuskunnan hallinnon kanssa. Yksikön organisaatio ja toiminta ohjautuvat näiden sääntöjen mukaisesti. Perusperiaatteena on, että kaikkien yksikössä asuvien tulee voida osallistua yksikön sääntöjen määrittelyyn. Säännöt tulee määritellä siten, että sekä Koto-osuuskunta että yksikkö ja sen ihmiset hyötyvät yksikön toiminnasta. Kuten aiemmin mainittiin, yksiköiden tulisi olla mahdollisimman autonomeja ja itseorganisoituvia, kun taas osuuskunnan hallinto astuu kuvaan ja ohjaa yksikön sisäisiä asioita vain tarvittaessa. Osuuskunta pyrkii kuitenkin tarjoamaan apua, ohjausta ja työkaluja yksiköiden mahdollisimman tehokkaaseen ja kestävään järjestäytymiseen.

Yksikössä asuvien ihmisten mahdolliset asemat määritellään yksikön sääntöjen sisällä. Jos nämä asemat oikeudellisesti koskevat osuuskuntaa, ne määritellään yhdessä osuuskunnan hallinnon kanssa. Tällaisia mahdollisia osuuskuntaa koskevia asemia ovat vuokralaiset, osuuskunnan työntekijät ja osuuskunnan omistajat.

Yksiköt perustaa osuuskunta, ja osuuskunta omistaa yksiköiden kiinteistöt. Osuuskunta voi myös lakkauttaa yksikön, jos yksikkö ei toimi ja kuluttaa osuuskunnan varoja. Yksiköt saavat varoja osuuskunnalta ja pyrkivät taloudelliseen kestävyyteen. Lopulta yksiköiden tulisi tuottaa voittoa, jota osuuskunta käyttää uusien yksiköiden rahoittamiseen. Tulot ja menot käsittelee osuuskunnan hallinto.

## Tulevaisuuden kehitys

Tulevaisuudessa Koto-osuuskunta haluaa kehittää hyviä tapoja järjestää tiimit ja yksiköt osuuskunnan sisällä. Hyviksi todetut lähestymistavat tulisi tiedottaa yksiköille ja tiimeille. Osuuskunta pyrkii tarjoamaan ohjausta tiimeille ja yksiköille siitä, miten ne voivat järjestäytyä ja toimia tehokkaasti ja kestävästi.

Koto-osuuskunta suosittelee kaikkien osuuskunnan tiimien ja yksiköiden määrittelevän toimintansa mahdollisimman selkeästi. Tiimeille tämä tarkoittaa tiimin tarkoituksen, tavoitteiden ja toimintaperiaatteiden määrittelyä. Yksiköille tämä tarkoittaa yksikön sääntöjen määrittelyä.

Yksi vielä avoinna oleva kysymys on, mitä yksikön sääntöjen tulisi vähintään sisältää?
