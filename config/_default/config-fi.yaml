title: Elokoto Osuuskunta
theme: koto
paginate: "4"
summaryLength: "15"
disqusShortname: ""
disableLanguages: []
defaultContentLanguage: fi

staticDir: 
  - static
  - assets
outputs:
  home:
    - HTML
    - RSS
  section:
    - HTML
    - RSS
params:
  plugins:
    css:
      - link: plugins/bootstrap/bootstrap.min.css
      - link: plugins/themify-icons/themify-icons.css
      - link: plugins/slick/slick.css
      - link: https://fonts.googleapis.com/css?family=Anaheim|Quattrocento+Sans:400,700&display=swap
    js:
      - link: plugins/bootstrap/bootstrap.bundle.min.js
      - link: plugins/shuffle/shuffle.min.js
      - link: plugins/lazy-load/lozad.min.js
      - link: plugins/google-map/map.js
  logo: images/logo.svg
  description: Moving towards post-scarcity world!
  author: Koto Co-op
  images:
    - /images/KOTO-logo_-2020-rgb_text-vert-white.png
  google_analytics_id: ""
  post_share: false
  preloader:
    enable: false
    preloader: images/logo.svg
  cookies:
    enable: false
    expire_days: 2
  map:
    enable: false
    gmap_api: https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ
    map_latitude: "51.5223477"
    map_longitude: "-0.1622023"
    map_marker: images/marker.png
  social:
    - icon: fa-brands fa-facebook
      link: https://facebook.com/kotocoop
    - icon: fa-brands fa-twitter
      link: https://twitter.com/kotocoop
    - icon: fa-brands fa-youtube
      link: https://tube.kotocoop.org/videos/local
    - icon: fa-brands fa-discord
      link: https://discord.gg/7eVJFCG
    - icon: fa fa-comments
      link: https://matrix.to/#/%23public:matrix.kotocoop.org
    - icon: fa-brands fa-telegram
      link: https://t.me/kotocoop
    - icon: fa-brands fa-instagram
      link: https://www.instagram.com/kotocoop
    - icon: fa-brands fa-instagram
      link: https://www.instagram.com/osuuskuntaelokoto
menu:
  main:
    - name: malli
      url: /model
      weight: 1
    - name: julkaisut
      url: /blog
      weight: 6
    - name: artikkelit
      url: /articles
      weight: 7
    - name: ota yhteyttä
      url: /contact
      weight: 8
    - name: support us
      url: /support
      weight: 9
    - name: code of conduct
      url: /code_of_conduct
      weight: 10
    - name: Auroora
      url: /units/auroora
      weight: 11
Languages:
  fi:
    languageName: Fi
    languageCode: fi-Fi
    contentDir: content/finnish
    weight: 1
    copyright:
      This work is licensed under a Creative Commons Attribution-ShareAlike
      4.0 International License.
    params:
      sections:
        earth_restoring_lifestyle:
          title: "Maan palauttava elämäntapa"
          id: earth_restoring_lifestyle
          image: /img/section/earth_restoring_lifestyle.jpg
          text: "Ilmasto- ja ympäristökatastrofi, jota kohtaamme, ei ratkea ilman elämäntapamuutosta. Maapallon pelastamisen pitäisi olla osa kaikkea, mitä teemme. Haluamme siirtää kukoistavan ja elinvoimaisen planeetan tuleville sukupolville. Elämäntapa, johon kuuluu paikallisesti hankittuja tuotteita, uusiutuvaa maataloutta ja älykästä luonnonvarojen käyttöä, on olennaisen tärkeä, mutta meidän on tehtävä siitä helppoa ihmisille."
          filtertag: earth_restoring_lifestyle
          subs:
            - title: "Ekosysteemeistä huolehtimisen tarkoituksenmukaisuuden löytäminen"
              id: purpose_ecosystems
              image: /img/subs/purpose_ecosystems.jpg
              content: "Ihmiset eivät ole onnellisia, ja monista ihmisistä jopa kierrättämisen yrittäminen tuntuu liian työläältä. väitämme, että voit löytää tarkoituksen ja merkityksen tekemällä työtä maapallon hyväksi ja auttamalla muita ihmisiä elämään terveellisempää, onnellisempaa ja ympäristöystävällisempää elämää. Ihmisiä ei pitäisi pakottaa elämään stressaavaa, kulutusta korostavaa elämäntapaa, kun on olemassa vaihtoehtoja, joita voimme tarjota heille."
            - title: "Kokonaisvaltainen hyvinvointi"
              id: holistic_wellbeing
              image: /img/subs/holistic_wellbeing.jpg
              content: "Koko itsestämme huolehtiminen, ei vain fyysisesti vaan myös henkisesti ja hengellisesti, on elintärkeää. Henkilökohtainen hyvinvointimme antaa meille mahdollisuuden osallistua tehokkaammin muiden ja planeettamme hoitamiseen. Terve maailma puolestaan tukee kokonaisvaltaista hyvinvointiamme."
            - title: "Osallistava yhteisö"
              id: inclusive_community
              image: /img/subs/inclusive_community.jpg
              content: "Ihmiset menestyvät, kun heidän ympärillään on joukko ihmisiä, jotka tukevat heitä päivittäisissä kamppailuissa. Yhteistyö ja keskinäinen ymmärrys ovat perustana vahvalle ryhmälle, joka kykenee luomaan myönteistä muutosta. Haluamme antaa ihmisille erilaisia tapoja antaa panoksensa, mutta odotamme heidän kertovan, mitä he haluavat saavuttaa, ja voimme yrittää toteuttaa sen yhdessä."
            - title: "Jaetaan maailma muiden kuin ihmiseläinten kanssa"
              id: sharing_with_animals
              image: /img/subs/sharing_with_animals.jpg
              content: "Jaamme tämän maapallon eläinten kanssa, jotka ovat olennainen osa maailmanlaajuista ekosysteemiämme. Hyväksymme sen, että myötätunnon edistäminen ja aiheuttamamme kärsimyksen vähentäminen on tärkeämpää kuin makumme tai tapojemme tyydyttäminen tai pienten epämukavuuksien välttäminen."
    
        open_source_sustainable_technology:
          title: "Avoimen lähdekoodin kestävä teknologia"
          id: open_source_sustainable_technology
          image: /img/section/open_source_sustainable_technology.jpg
          text: "Kestävien teknologioiden käyttöönotto mahdollistaa tulevaisuuden, jossa edistys ja ympäristönsuojelu kulkevat käsi kädessä. Avoimen lähdekoodin ja vihreät innovaatiot edustavat yhteistoiminnallista edistysaskelta, jossa korostuvat kollektiivinen viisaus ja jaettu vastuu."
          filtertag: open_source_sustainable_technology
          subs:
            - title: "Teknologian kehitys"
              id: technology_development
              image: /img/subs/technology_development.jpg
              content: "Teknologinen kehitys, jos se on ympäristöystävällistä, voi tukea ympäristöystävällistä elämäntapaa ja helpottaa ihmisten elämää. Luontoa suojelevien teknologioiden avulla voimme säilyttää nykyaikaiset mukavuudet vaarantamatta tulevien sukupolvien terveyttä."
            - title: "Avoin yhteistyö ja tiedon jakaminen"
              id: open_collaboration
              image: /img/subs/open_collaboration.jpg
              content: "Tietämyksen jakaminen lisää kollektiivista älykkyyttä ja innostaa innovatiivisiin ratkaisuihin globaaleihin haasteisiin. Avoimen yhteistyön kautta me kaikki hyödymme jaetuista ideoista ja kokemuksista. Yritysten ei pitäisi estää uusia innovaatioita."
            - title: "Uusiutuvat teknologiat ja ympäristövastuulliset käytännöt"
              id: renewable_technologies
              image: /img/subs/renewable_technologies.jpg
              content: "Sitoutuminen uusiutuviin energialähteisiin ja vastuulliseen resurssienhallintaan on olennaisen tärkeää ympäristövaikutuksiemme minimoimiseksi. Ympäristötietoisilla käytännöillä varmistetaan luonnon jatkuva kyky tukea elämää."
            - title: "Elämäntapojen vaikutusten arvioinnit ja tutkimukset"
              id: impact_assessments
              image: /img/subs/impact_assessments.jpg
              content: "Elämäntapojemme ympäristövaikutusten arvioiminen antaa meille mahdollisuuden tehdä tietoon perustuvia päätöksiä. Tutkimus ja tiedonvälitys näistä tuloksista ohjaavat meitä kohti kestävämpiä elämän- ja työskentelytapoja."
            - title: "Matalan teknologian innovointi: Yksinkertaisuuden juhlistaminen"
              id: low_tech_innovation
              image: /img/subs/low_tech_innovation.jpg
              content: "Yksinkertainen on usein parasta. Matalan teknologian ratkaisut, kuten sadeveden kerääminen ja luonnonmukaiset tai kierrätetyt rakennusmateriaalit, voivat olla sekä tehokkaita että ympäristöystävällisiä, sillä ne toimivat yhdessä luonnon kanssa aiheuttamatta kohtuutonta haittaa."
    
        organisation_supporting_change:
          title: "Organisaatio, joka tukee ihmisiä luomassa muutosta"
          id: organisation_supporting_change
          image: /img/section/organisation_supporting_change.jpg
          text: "Koto Co-opin tarkoituksena on voittoa tavoittelemattomana organisaationa mahdollistaa kasvun avulla yhä useampien ihmisten osallistuminen muutokseen. Toivomme, että auttamalla ihmisiä ja maapalloa teemme voittoa, joka kierrätetään yhä useampien ihmisten auttamiseksi. Tämän positiivisen palautteen avulla haluamme varmistaa, että tarpeeksi moni ihminen pääsee irti kapitalistisesta järjestelmästä ja maapallo voidaan pelastaa."
          filtertag: organisation_supporting_change
          subs:
            - title: "Yksiköt"
              id: units
              image: /img/subs/units.jpg
              content: "Yksikkömme ovat ihmisryhmiä, jotka haluavat tehdä toimintaa, joka auttaa ihmisiä paikallisesti ja auttaa osuuskuntaa. Yksikkö voi olla Aurooran kaltainen ekologisen elämän ja teknologian keskus, yhteisöllinen puutarha, hackerspace tai vegaaninen katuruokakauppa. Jäsenemme voivat käyttää yksikön tarjoamia resursseja yksikön ja osuuskunnan yhdessä laatimien sääntöjen mukaisesti."
            - title: "Hajautettu organisaation kehittäminen"
              id: decentralised_development
              image: /img/subs/decentralised_development.jpg
              content: "Hajautettu organisaatiorakenne arvostaa jokaisen panosta, kannustaa yhteiseen päätöksentekoon ja yhteenkuuluvuuden tunteeseen, mikä johtaa viisaampiin päätöksiin ja yhtenäisempään ryhmädynamiikkaan."
            - title: "Liiketoiminnan kasvu ja voitto"
              id: business
              image: /img/subs/business.jpg
              content: "Suomelaisena osuuskuntana olemme voittoa tavoittelematon organisaatio. Kaikki tuottamamme voitto sijoitetaan takaisin osuuskuntaan ja sitä ympäröiviin ihmisiin. Tuloja syntyy soveltamalla ja edistämällä avoimen lähdekoodin ratkaisuja, tarjoamalla palveluja muille organisaatioille ja tarjoamalla asumisratkaisuja kestävillä käytännöillä. Pohjimmiltaan osuuskunnan liiketoiminta on yhtä lailla sen mission tukemista kuin myönteisen muutoksen aaltovaikutuksen luomista maailmanlaajuisten yhteistyökumppaneiden verkoston kautta."
