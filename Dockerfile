FROM registry.gitlab.com/pages/hugo/hugo_extended:latest@sha256:ff07c15f0ff5876c7a737d07b906d48802e7fcd1bba02f648fae63fda57ddb56

ARG UID=1000
ENV HOST_UID=${UID}

RUN whoami
RUN export
RUN echo jee ${UID}

RUN adduser -u ${HOST_UID} -D hugo
RUN adduser hugo bin

USER 1000

ENTRYPOINT hugo serve --debug -d /tmp/public --cleanDestinationDir --disableFastRender
