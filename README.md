# Koto co-op Website

Koto co-op website is build with Hugo static website generator and currently hosted on gitlab -pages.

## Install Hugo

For Linux Hugo can be usually installed with the tools found in the distro (apt-get, snap and so on).

Installation instructions are found here [https://gohugo.io/getting-started/installing/](https://gohugo.io/getting-started/installing/). 


## Clone the repository

The theme used is a submodule of website repository so clone it recursevily. 

	git clone git@gitlab.com:kotocoop/kotocoop-website.git --recursive
	
Of course use your own fork, if you prefer to.

## Build or serve the site

to build the website ones you just run:

	hugo

and the website will be found in the "public" folder.

You can run a hugo server locally and access that the site with a browser:

	hugo serve

and access [http://localhost:1313](http://localhost:1313)
If you use the server option, sit will be built automatically and browser will be refreshed everytime you save your modifications.

## Running a fork

You can fork the site in Gitlab and push your modifications there. For gitlab pages to work, you have to change baseURL setting in config.toml

	baseURL = "/kotocoop-website/"
	
After that you access your fork like for example [https://jeukku.gitlab.io/kotocoop-website/](https://jeukku.gitlab.io/kotocoop-website/)


